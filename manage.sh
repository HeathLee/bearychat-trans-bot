#!/bin/bash

Version="1.1"
CurDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ -a ${CurDir}/env.sh ]]; then
  source ${CurDir}/env.sh
fi

start() {

  docker kill trans_bot 2>/dev/null
  docker rm -v trans_bot 2>/dev/null

  docker run -d --name trans_bot \
    -e "AZURE_TRANS_API_KEY=${AZURE_TRANS_API_KEY}" \
    --net=host \
    --restart=always \
    --log-opt max-size=10m \
    --log-opt max-file=9 \
    ycli/bearychat:${Version} \
    gunicorn -w 4 -b 127.0.0.1:8080 server:app
}

stop() {
  docker stop trans_bot 2>/dev/null
  docker rm -v trans_bot 2>/dev/null
}

reload() {
  docker kill -s HUP trans_bot
}

build-img() {
    docker build -t ycli/bearychat:${Version} .
    docker push ycli/bearychat:${Version}
}

nginx() {
    docker kill nginx 2>/dev/null
    docker rm -v nginx 2>/dev/null
    docker run -d --name nginx \
    -v ${CurDir}/nginx.conf:/etc/nginx/nginx.conf:ro \
    --net=host \
    --restart=always \
    --log-opt max-size=10m \
    --log-opt max-file=9 \
    nginx:latest
}


##################
# Start of script
##################

Action=$1

shift

case "$Action" in
  start) start "$@";;
  stop) stop ;;
  reload) reload "$@" ;;
  nginx) nginx "$@" ;;
  build-img) build-img "$@" ;;
  restart)
    stop
    start
    ;;
  *)
    echo "Usage:"
    echo "./manage.sh start|stop|restart"
    echo "./manage.sh reload"
    echo "./manage.sh build-img"
    echo "./manage.sh nginx"
    ;;
esac

exit 0
