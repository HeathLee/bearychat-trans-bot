from flask import Flask, jsonify, request
from trans import translator

app = Flask(__name__)

@app.route('/beary/bot/trans', methods=['POST'])
def translate():
    data = request.get_json()
    text = data['text']
    token = data['token']
    if token not in [
        'aad27e91ab4e80e9eb0bb3c1c73b9481',
        'f1b75dd97bf90fb81e6fc2e0abd8f840'
    ]:
        return jsonify({
            'text': 'error token'
        })
    if text:
        text = text.lstrip(data['trigger_word']).strip()
        res, text = translator.trans(text)
        if not res:
            text = 'something wrong'
    else:
        text = 'something wrong'
    return jsonify({
        'text': text
    })

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8080)
