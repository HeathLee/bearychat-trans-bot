import os, requests, uuid, json, re

class Translator():

    def __init__(self, key):
        self.url = 'https://api.cognitive.microsofttranslator.com/translate'
        self.client = requests.Session()
        self.client.headers.update({
            'Ocp-Apim-Subscription-Key': key,
            'Content-type': 'application/json',
            'X-ClientTraceId': str(uuid.uuid4())
        })
        self.cn_pattern = re.compile(u'[\u4e00-\u9fa5]+')

    def check_cn(self, text):
        """
        检测是否含有中文
        """
        return bool(self.cn_pattern.search(text))

    def trans(self, text):
        body = [{
            'text' : text
        }]
        res = self.client.post(
            self.url,
            params={
                "api-version": "3.0",
                "to": "en" if self.check_cn(text) else "zh-Hans"
            },
            json=body,
            timeout=2
        )
        data = res.json()
        if res.status_code != 200:
            return False, data['message']
        return True, data[0]['translations'][0]['text']


translator = Translator(key=os.environ['AZURE_TRANS_API_KEY'])
