FROM       python:3.5
RUN        pip install flask gunicorn requests -i https://mirrors.aliyun.com/pypi/simple
COPY       . /app
WORKDIR    /app
CMD        ["python", "server.py"]
